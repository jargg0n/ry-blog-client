import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Profile } from '../profile';
import { FormGroup, FormControl} from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  isLoggedIn: boolean;

  editMode: boolean = false;

  profile: Profile = {};

  editForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl('')
    //email: new FormControl('')
  });

  constructor(
    private appService:AppService
    ) { }

  ngOnInit() {
    this.isLoggedIn = this.appService.checkCredentials();  

    if (this.isLoggedIn){
      this.appService.getUserResource('current')
      .subscribe(
        (data: Profile) => this.profile = {
          id: data['id'],
          firstName: data['firstName'],
          lastName: data['lastName'],
          email: data['email'],
          enabled: data['enabled']
        });
    }  
  }

  edit(){
    this.editMode = true;
    this.editForm.get('firstName').setValue(this.profile.firstName);
    this.editForm.get('lastName').setValue(this.profile.lastName);
    //this.editForm.get('email').setValue(this.profile.email);
  }

  onSubmit() {
    let profile:Profile = {
      id: this.profile.id,
      firstName: this.editForm.get('firstName').value,
      lastName: this.editForm.get('lastName').value,
      email: this.profile.email,
      enabled: this.profile.enabled
    }
    this.appService.editProfile(profile).subscribe(data => this.reload(), error => console.log(error));
  }

  reload(){
    location.reload();
  }

}
