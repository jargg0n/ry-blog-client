import { Component, OnInit } from '@angular/core';
import { Post } from '../post';
import { Profile } from '../profile';
import { AppService } from '../app.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {

  constructor(
    private appService:AppService,
    private router: Router
    ) { }

  profile:Profile = {
    firstName: '',
    lastName: ''
  };

  isLoggedIn: boolean;

  postForm = new FormGroup({
    content: new FormControl('')
  });

  ngOnInit() {
    this.isLoggedIn = this.appService.checkCredentials();  

    if (this.isLoggedIn){
      this.appService.getUserResource('current')
      .subscribe(
        (data: Profile) => this.profile = {
          id: data['id'],
          firstName: data['firstName'],
          lastName: data['lastName'],
          email: data['email'],
          enabled: data['enabled']
        });
    }  
  }

  onSubmit() {
    let post:Post = {
      content: this.postForm.get('content').value
    }
    this.appService.createPost(post).subscribe(data => this.router.navigate(['dashboard']), error => console.log(error));
  }

}
