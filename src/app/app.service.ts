import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Profile } from './profile';
import { Registration } from './registration';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Post } from './post';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(
    private http: HttpClient,
    private cookie: CookieService
    ) { }

    // profile: Profile;

  getUserResource(resourceUrl) : Observable<any>{
    var headers = new HttpHeaders({'Content-type': 'application/x-www-form-urlencoded; charset=utf-8', 'Authorization': 'Bearer ' + this.cookie.get('access_token')});
    return this.http.get(environment.userApiUrl + resourceUrl, { headers: headers });
  }

  editProfile(profile: Profile) : Observable<any>{
    var headers = new HttpHeaders({'Content-type': 'application/json', 'Authorization': 'Bearer ' + this.cookie.get('access_token')});
    return this.http.patch<Profile>(environment.userApiUrl + 'profiles/' + profile.id, profile, { headers: headers });
  }

  retrieveToken(code: string){
    let params = new URLSearchParams();   
    params.append('grant_type','authorization_code');
    params.append('client_id', environment.clientId);
    params.append('client_secret', environment.clientSecret);
    params.append('redirect_uri', environment.redirectUri);
    params.append('code', code);

    let headers = new HttpHeaders({'Content-type': 'application/x-www-form-urlencoded; charset=utf-8', 'Authorization': 'Basic ' + btoa(environment.clientId + ':' + environment.clientSecret)});
    
    this.http.post(environment.userApiUrl + 'oauth/token', params.toString(), { headers: headers })
    .subscribe(
      data => this.saveToken(data),
      err => alert('Invalid Credentials')
    ); 
  }

  // getProfile(): Profile{
  //   if (this.profile == null && this.checkCredentials){
  //     this.getUserResource('current').subscribe(
  //       (data: Profile) => this.profile = {
  //         id: data['id'],
  //         firstName: data['firstName'],
  //         lastName: data['lastName'],
  //         email: data['email'],
  //         enabled: data['enabled']
  //       });
  //     return this.profile;
  //   } else {
  //     return this.profile;
  //   }
  // }

  saveToken(token){
    var expireDate = new Date().getTime() + (1000 * token.expires_in);
    this.cookie.set("access_token", token.access_token, expireDate);
    console.log('Obtained Access token');
    window.location.href = environment.baseUrl;
  }

  checkCredentials(){
    return this.cookie.check('access_token');
  } 

  logout() {
    this.cookie.delete('access_token');
    window.location.href = environment.userApiUrl + 'logout';
  }

  signup(registration:Registration){
    var headers = new HttpHeaders({'Content-type': 'application/x-www-form-urlencoded; charset=utf-8', 'Authorization': 'Bearer ' + this.cookie.get('access_token')});
    return this.http.post<Profile>(environment.userApiUrl + 'signup', registration, {headers: headers});
  }

  getAllPosts() {
    return this.http.get(environment.postApiUrl + 'posts?page=0&size=' + 9999);
  }

  // getPosts(page: number, pageSize: number) {
  //   return this.http.get(environment.postApiUrl + 'posts?page=' + page + '&size=' + pageSize);
  // }

  createPost(post:Post){
    var headers = new HttpHeaders({'Content-type': 'application/json', 'Authorization': 'Bearer ' + this.cookie.get('access_token')});
    return this.http.post<Post>(environment.postApiUrl + 'posts', post, {headers: headers});
  }

  getMyPosts(){
    this.getUserResource('current').subscribe(
      data => {
        this.http.get(environment.postApiUrl + 'posts/' + data['id']).subscribe(
          data => {
            return data['content'];
          }
        );
      });
  }
}
